import React from "react";
import ReactDOM from "react-dom";
import Index from "./pages/index";

const App = () => {
  return (
    <>
      <Index />
    </>
  );
};
ReactDOM.render(
  <h1>
    <App />
  </h1>,
  document.getElementById("root")
);
